import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      term: ''
    }

  }

  onInputChange = (event) => {
    this.setState({ term : event.target.value });
  }

  onFormSubmit = (event) => {
    event.preventDefault();
    {this.props.fetchWeather(this.state.term)}
  }


  render () {
    return (
      <form
        className="input-group"
        onSubmit={this.onFormSubmit}
      >
        <input
          className="form-control"
          onChange={this.onInputChange}
          value={this.state.term}
          placeholder="Add new US city"
        />
        <span className="input-group-btn">
          <button className="btn btn-secondary">Submit</button>
        </span>
      </form>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators( { fetchWeather }, dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);